package banking;

import java.util.Scanner;


public class MyBanking {
	public double accBal = 0.0;

	public void deposit(double depAmt) {
		accBal = accBal + depAmt;
	}

	public void widthdraw(double widAmt) {
		accBal = accBal - widAmt;
	}

	public static void main(String[] args) {
		MyBanking bank = new MyBanking();
		Scanner scan = new Scanner(System.in);
		System.out.println("Welcome to Ceylon Banking System");

		MyBanking Bank = new MyBanking();
		double num, num1, num2, num3, num4 = 0;

		do {
			System.out.print("Enter your current balance :");
			num = scan.nextDouble();
			if (num < 0) {
				System.out.println(" ***Beginning balance must be at least zero,please re-enter.***");
			}
		} while (num < 0);
		bank.accBal = num;

		do {
			System.out.print("Enter the number of deposits(0-5):");
			num1 = scan.nextDouble();
			if (num1 > 5) {
				System.out.println("*** Invalid number of deposits,please re-enter.Enter the number of deposits:3***");
			}
		} while (num1 > 5);

		do {
			System.out.print("Enter the number of withdrawals(0-5):");
			num2 = scan.nextDouble();
			if (num2 > 5) {
				System.out.println("*** deposit amount must be greater than zero,please re-enter ***");
			}
		} while (num2 > 5);

		for (int i = 1; i <= num1; i++) {
			System.out.print("Enter the amount of deposit " + i + " :");
			num3 = scan.nextDouble();
			bank.deposit(num3);
		}
		System.out.println("Bank balance is : " + bank.accBal);

		do {
			for (int i = 1; i <= num2; i++) {
				System.out.print("Enter the amount of withdrawal " + i + " :");
				num4 = scan.nextDouble();
				if (num4 > bank.accBal) {
					System.out.println("***withdrawal amount exceeds current balance,please re-enter");
				}

				bank.widthdraw(num4);
			}
		} while (num2 > 5);
		System.out.println("Bank balance is : " + bank.accBal);

	}
}